## Distributed Traceing Demo ##

We will be adding the spring cloud sleuth for all four microservices. Spring Cloud Sleuth will add a unique token to all requests. Spring Cloud Sleuth used to generate and attach the trace id, span id to the logs. Later, tools like Zipkin, ELK etc. can use for storage and analysis.

![schema](https://bitbucket.org/min2ha/spring-cloud-sleuth-zipkin/raw/master/img/chain2.jpg)



#### Quick summary ####
Distributed Log Tracing demo with Spring-Cloud-Sleuth + Zipkin:

* Spring Cloud Sleuth is used to generate and attach the trace id, span id to the logs so that these can then be used by tools like Zipkin and ELK for storage and analysis
* Zipkin is a distributed tracing system. It helps gather timing data needed to troubleshoot latency problems in service architectures. Features include both the collection and lookup of this data.



### How do I get set up? ###

#### Summary of set up ####
```
docker run -d -p 9411:9411 openzipkin/zipkin
```

Run all 4 microservices in parallel

Open browser and Call microservice 1 and initiate chain of 4:

* http://localhost:8080/microservice1

Observe distributed trace timings:
* http://localhost:9411/zipkin/


![screenshot](https://bitbucket.org/min2ha/spring-cloud-sleuth-zipkin/raw/master/img/zipkin.jpg)


